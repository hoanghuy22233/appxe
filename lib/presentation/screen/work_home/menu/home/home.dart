import 'package:flutter/material.dart';
import 'package:ybc/app/constants/color/color.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_not_search.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_with_search.dart';

class WorkHomePage extends StatefulWidget {
  @override
  _WorkHomePageState createState() => _WorkHomePageState();
}

class _WorkHomePageState extends State<WorkHomePage>
    with AutomaticKeepAliveClientMixin<WorkHomePage> {


  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return
      Scaffold(
        backgroundColor: Colors.grey[300],
        body: SafeArea(
            top: true,
            child: Column(
              children: [
                _buildAppbar("Trang chủ"),
                Expanded(
                  child: Center(
                    child: Container(
                      child: Text('Trang chủ'),
                    ),
                  ),
                )
              ],
            )),
      );
  }

  @override
  bool get wantKeepAlive => true;

  Widget _buildAppbar(String title) =>
      WidgetAppbarNotSearch(
        backgroundColor: AppColor.PRIMARY_COLOR,
        textColor: Colors.white,
        title: title,
      );

}

