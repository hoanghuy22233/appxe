import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:ybc/app/constants/color/color.dart';
import 'package:ybc/model/entity/barrel_entity.dart';
import 'package:ybc/model/entity/category_menu.dart';
import 'package:ybc/model/repo/home_repository.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_with_search.dart';
import 'package:ybc/presentation/common_widgets/widget_spacer.dart';



class WorkNewsScreen extends StatefulWidget {
  @override
  _WorkNewsScreenState createState() => _WorkNewsScreenState();
}

class _WorkNewsScreenState extends State<WorkNewsScreen>
    with AutomaticKeepAliveClientMixin<WorkNewsScreen> {

  String dropdownValue;
  String name, create;

  List<Item> _item = Item.getItem();
  List<DropdownMenuItem<Item>> _dropdownMenuItems;
  Item _selected;
  HomeData dropdownValues;
  List<HomeData> homeDatas;
  int type;

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    openLoading();
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    return
      Scaffold(
        backgroundColor: Colors.grey[300],
        body: SafeArea(
            top: true,
            child: Column(
              children: [
                _buildAppbar("Tin tức", "Tìm kiếm tin tức"),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  height: 35,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // Expanded(
                      //   flex: 6,
                      //   child: BlocBuilder<HomeCategoriesBloc,
                      //       HomeCategoriesState>(
                      //     builder: (context, state) {
                      //       if (state is HomeCategoriesLoaded) {
                      //         return Container(
                      //           padding:
                      //           EdgeInsets.symmetric(horizontal: 15),
                      //           decoration: BoxDecoration(
                      //             borderRadius: BorderRadius.circular(25),
                      //             color: Colors.white,
                      //           ),
                      //           child: _builDropBoxs(state),
                      //         );
                      //       } else {
                      //         return Center(
                      //             child: Container(
                      //               height: 100,
                      //               width: 100,
                      //               child: Lottie.asset(
                      //                 'assets/lottie/trail_loading.json',
                      //               ),
                      //             ));
                      //       }
                      //     },
                      //   ),
                      // ),
                      SizedBox(
                        width: 10,
                      ),
                      Image.asset(
                        'assets/images/ic_grid.png',
                        height: 15,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      DropdownButton<Item>(
                        hint: Text("Sắp xếp theo"),
                        value: _selected,
                        icon: Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        elevation: 16,
                        underline: SizedBox(),
                        style: TextStyle(color: Colors.black),
                        onChanged: (Item value) {
                          setState(() {
                          });
                        },
                        items: _item.map((Item user) {
                          return DropdownMenuItem<Item>(
                            value: user,
                            child: Text(
                              user.name,
                              style: TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Container(
                      child: Text('Tin tức'),
                    ),
                  ),
                )
              ],
            )),
      );
    //   MultiBlocProvider(
    //   providers: [
    //     BlocProvider(
    //       create: (context) => HomeCategoryBloc(homeRepository: homeRepository),
    //     ),
    //     BlocProvider(
    //       create: (context) =>
    //           HomeCategoriesBloc(homeBloc: BlocProvider.of<HomeBloc>(context)),
    //     ),
    //   ],
    //   child: BlocListener<NewsBloc, NewsState>(
    //     listener: (context, state) async {
    //       if (state.isLoading) {
    //         await HttpHandler.resolve(status: state.status);
    //       }
    //
    //       if (state.isSuccess) {
    //         await HttpHandler.resolve(status: state.status);
    //       }
    //
    //       if (state.isFailure) {
    //         await HttpHandler.resolve(status: state.status);
    //       }
    //     },
    //     child: BlocBuilder<NewsBloc, NewsState>(
    //       builder: (context, state) {
    //         return Scaffold(
    //           backgroundColor: Colors.grey[300],
    //           body: SafeArea(
    //               top: true,
    //               child: Column(
    //                 children: [
    //                   _buildAppbar("Tin tức", "Tìm kiếm tin tức"),
    //                   Container(
    //                     margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
    //                     height: 35,
    //                     child: Row(
    //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                       children: [
    //                         Expanded(
    //                           flex: 6,
    //                           child: BlocBuilder<HomeCategoriesBloc,
    //                               HomeCategoriesState>(
    //                             builder: (context, state) {
    //                               if (state is HomeCategoriesLoaded) {
    //                                 return Container(
    //                                   padding:
    //                                       EdgeInsets.symmetric(horizontal: 15),
    //                                   decoration: BoxDecoration(
    //                                     borderRadius: BorderRadius.circular(25),
    //                                     color: Colors.white,
    //                                   ),
    //                                   child: _builDropBoxs(state),
    //                                 );
    //                               } else {
    //                                 return Center(
    //                                     child: Container(
    //                                   height: 100,
    //                                   width: 100,
    //                                   child: Lottie.asset(
    //                                     'assets/lottie/trail_loading.json',
    //                                   ),
    //                                 ));
    //                               }
    //                             },
    //                           ),
    //                         ),
    //                         SizedBox(
    //                           width: 10,
    //                         ),
    //                         Image.asset(
    //                           'assets/images/ic_grid.png',
    //                           height: 15,
    //                         ),
    //                         SizedBox(
    //                           width: 10,
    //                         ),
    //                         DropdownButton<Item>(
    //                           hint: Text("Sắp xếp theo"),
    //                           value: _selected,
    //                           icon: Icon(Icons.arrow_drop_down),
    //                           iconSize: 24,
    //                           elevation: 16,
    //                           underline: SizedBox(),
    //                           style: TextStyle(color: Colors.black),
    //                           onChanged: (Item value) {
    //                             setState(() {
    //                               _selected = value;
    //                               BlocProvider.of<NewsBloc>(context).add(
    //                                   LoadNews(
    //                                       widget?.type ?? null,
    //                                       _selected?.key ?? '',
    //                                       _selected?.key ?? ''));
    //                               print("____________");
    //                               print(_selected.key);
    //                             });
    //                           },
    //                           items: _item.map((Item user) {
    //                             return DropdownMenuItem<Item>(
    //                               value: user,
    //                               child: Text(
    //                                 user.name,
    //                                 style: TextStyle(color: Colors.black),
    //                               ),
    //                             );
    //                           }).toList(),
    //                         )
    //                       ],
    //                     ),
    //                   ),
    //                   Expanded(
    //                     child: _buildContent(state),
    //                   )
    //                 ],
    //               )),
    //         );
    //       },
    //     ),
    //   ),
    // );
  }


  @override
  bool get wantKeepAlive => true;

  Widget _buildAppbar(String title, String titleSearch) =>
      WidgetAppbarWithSearch(
        backgroundColor: AppColor.PRIMARY_COLOR,
        textColor: Colors.white,
        title: title,
        titleSearch: titleSearch,
      );
  void openLoading() async {
    Future.delayed(Duration(seconds: 2), () {

    });
  }
}

class Item {
  const Item(this.id, this.name, this.key);
  final int id;
  final String name;
  final String key;

  static List<Item> getItem() {
    return <Item>[
      Item(0, 'Lọc theo tên', 'name|asc'),
      Item(1, 'Lọc theo ngày', "created_at|desc"),
    ];
  }
}
