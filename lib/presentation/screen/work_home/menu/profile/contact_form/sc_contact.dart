import 'package:flutter/material.dart';
import 'package:ybc/app/constants/value/value.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:ybc/presentation/common_widgets/widget_appbar_not_search.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/contact_form/widget_contact_form.dart';
import 'package:ybc/utils/locale/app_localization.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              _buildAppbar(AppLocalizations.of(context).translate('register_contact.title')),
              Expanded(
                child: Container(
                  //height: MediaQuery.of(context).size.height-140,
                  padding: EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start ,
                      children: [
                        _buildImage(),
                        WidgetContactForm(),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildImage(){
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 0),
      child: Row(
        children: [
          Image.asset(AppValue.APP_LOGO,
              //  color: Colors.white.withOpacity(0.8),
              height: 70,
              width: 70),
          SizedBox(
            width: 25,
          ),
          Text(
            "TEMIS Base code",
            style: TextStyle(
                fontSize: 24,
                color: Colors.blue,
                fontFamily: 'Righteous'),
          ),
        ],
      ),
    );
  }

  Widget _buildAppbar(String title) => WidgetAppbarNotSearch(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        left: [
          WidgetAppbarMenuBack(),
        ],
        title: title,
      );
}
