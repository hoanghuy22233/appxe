import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ybc/model/repo/barrel_repo.dart';
import 'package:ybc/presentation/screen/forgot_pass/forgot_password_sc.dart';
import 'package:ybc/presentation/screen/forgot_pass_reset/forgot_password_reset_sc.dart';
import 'package:ybc/presentation/screen/login/sc_login.dart';
import 'package:ybc/presentation/screen/register/sc_register.dart';
import 'package:ybc/presentation/screen/register_vefical/register_verify_sc.dart';
import 'package:ybc/presentation/screen/splash/sc_splash.dart';
import 'package:ybc/presentation/screen/thought/BHWalkThroughScreen.dart';
import 'package:ybc/presentation/screen/work_home/cart/sc_cart.dart';
import 'package:ybc/presentation/screen/work_home/menu/plan_business/create_sample_plan_business_one/sc_create_sample_plan_business.dart';
import 'package:ybc/presentation/screen/work_home/menu/plan_business/create_sample_plan_business_two/sc_create_sample_plan_business_two.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/bought_product/sc_bought_product.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/change_password/change_password_sc.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/contact_form/sc_contact.dart';
import 'package:ybc/presentation/screen/work_home/menu/profile/information_personal/bloc/profile_bloc.dart';
import 'package:ybc/presentation/screen/work_home/work_navigation/work_navigation.dart';

import 'screen/work_home/menu/plan_business/template_choose/sc_template_choose.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String REGISTER = 'register';
  static const String FORGOT_PASS = 'forgot_pass';
  static const String FORGOT_PASS_VERIFY = 'forgot_pass_verify';
  static const String REGISTER_VERIFY = 'register_verify';
  static const String FORGOT_PASS_RESET = 'forgot_pass_reset';
  static const String CHANGE_PASSWORD = 'change_password';
  static const String MENU = 'menu';
  static const String THOUGHT = 'thought';
  static const String WORK_SERVICE = 'work_service';
  static const String NAVIGATION = '/navigation';
  static const String WORK_NAVIGATION = '/work_navigation';
  static const String CREATE_SAMPLE_PLAN = '/create_sample_plan';
  static const String CREATE_SAMPLE_PLAN_TWO = '/create_sample_plan_two';
  static const String CHOOSE_TEMPLATE = '/choose_Template';
  static const String INFORMATION_ACCOUNT = '/information_account';
  static const String BOUGHT_PRODUCT = '/bought_product';
  static const String HISTORY_ORDER = '/history_order';
  static const String CHANGE_PROFILE = '/change_profile';
  static const String CART = '/cart';
  static const String SEARCH = '/search';
  static const String NEWS_DETAIL = '/new_detail';
  static const String ACCOUNT = '/account';
  static const String SEARCH_COURSER = '/search_courser';
  static const String DETAIL_COURSER = '/detail_courser';

  static const String REGISTER_CONTACT = '/register_contact';


  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case THOUGHT:
        return MaterialPageRoute(builder: (_) => BHWalkThroughScreen());
      case WORK_NAVIGATION:
        return MaterialPageRoute(builder: (_) => WorkNavigationScreen());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      THOUGHT: (context) => BHWalkThroughScreen(),
      // WORK_NAVIGATION: (context) => WorkNavigationScreen(),
      REGISTER: (context) => RegisterScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyPage(),
      LOGIN: (context) => LoginScreen(),
      FORGOT_PASS: (context) => ForgotPasswordScreen(),
      FORGOT_PASS_RESET: (context) => ForgotPasswordResetScreen(),
      CREATE_SAMPLE_PLAN: (context) => CreateSamplePlanScreen(),
      CREATE_SAMPLE_PLAN_TWO: (context) => CreateSamplePlanScreenTwo(),
      CHOOSE_TEMPLATE: (context) => ChooseTemplateScreen(),
      BOUGHT_PRODUCT: (context) => BoughtProductScreen(),
      CHANGE_PASSWORD: (context) => ChangePasswordScreen(),
      REGISTER_CONTACT: (context) => ContactScreen(),
      CART: (context) => CartScreen(),

      WORK_NAVIGATION: (context) => MultiBlocProvider(providers: [

            BlocProvider(
              create: (context) => ProfileBloc(userRepository: userRepository),
            ),
          ], child: WorkNavigationScreen()),

    };
  }
}
